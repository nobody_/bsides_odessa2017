#!/usr/bin/env python
import hashpumpy, cfscrape, requests

url           = 'https://securitybsides.org.ua/firewall.php'
original_mac  = '59e5bea232f682d7722d55d4e6795a246e6a486b'
original_data = 'user=n00b'

scraper    = cfscrape.create_scraper()
cookie, ua = scraper.get_cookie_string(url)

for i in range(0, 20):

  (sign, data) = hashpumpy.hashpump(original_mac, \
      original_data, \
      '&user=h4x0r', \
      i
  )

  req = requests.post(url, headers = {
    'X-MSG-AUTH-CODE': sign,
    'Cookie':          cookie,
    'User-Agent':      ua,
    'Content-Type':    'application/x-www-form-urlencoded',
  }, data = data)

  if 'LOL nice try :D' not in req.text:
    break

print(req.text)
