set breakpoint pending off
set pagination off
set logging on

def _loop
  set $i = 0
  while ( $i < $arg0 )
    set $i = $i + 1
    i r ecx edx
    set $ecx=$edx
    cont
  end
end

break *0x401273
command
  _loop 1
end

break *0x401331
command
  _loop 9
end

run </dev/null
quit
