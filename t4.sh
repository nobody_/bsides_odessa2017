#!/bin/bash
rm -f ./t4.exe ./gdb.txt

upx -d -o./t4.exe ./task4.exe >/dev/null
wineconsole --backend=curses /usr/i686-w64-mingw32/bin/gdb.exe --batch -x ./t4.gdb.ex ./t4.exe >/dev/null 2>/dev/null

FLAG=$(grep '^edx' ./gdb.txt | cut -f2 | tr -d '\r' | xargs -i printf '%x' '{}' | xxd -r -p)
echo "$FLAG"
