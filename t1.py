#!/usr/bin/env python2
import requests, sys, re

def xprint(s):
  return
  print(':'.join(_.encode('hex') for _ in s))

def reg(u):
  req = requests.post('http://46.101.255.144/task1/register', data = 'username='+u+'&password='+u+'&password_confirm='+u, headers = {'Connection': 'close', 'Content-Type': 'application/x-www-form-urlencoded'})
  c = req.headers['Set-Cookie']
  c = re.search(r'user=([^;]*);', c).group(1)[1:-1]
  return c.decode('base64')

login = 'A' * (5 + 14)
a = reg(login)
b = reg('BBBBB')
i = 0

while True:
  if a[i] == b[i]:
    i += 1
    continue
  break

old_a = a
a = a[i:i + len(login)]
b = b[i:i + len(login)]

key = ''
for k, v in enumerate(a):
  key += chr( ord(v) ^ ord(login[k]) )

xprint(key)

need = 'admin;userid=000001'

got = ''
for k, v in enumerate(key):
  got += chr( ord(v) ^ ord(need[k]) )

#xprint(a)
#xprint(got)

last = old_a[:i] + got + old_a[i+len(login):]

xprint(old_a)
xprint(last)

last = last.encode('base64')[:-1]
old_a = old_a.encode('base64')[:-1]

#print requests.get('http://46.101.255.144/task1/home', cookies = {'user': '"%s"' % old_a}).text
print requests.get('http://46.101.255.144/task1/home', cookies = {'user': '"%s"' % last}).text
